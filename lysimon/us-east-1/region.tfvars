# Specify the region parameter here
aws_region = "us-east-1"

# Add all the modules that should be reference here
alb_remote_state_key = "lysimon/us-east-1/alb/terraform.tfstate"
vpc_remote_state_key = "lysimon/us-east-1/vpc/terraform.tfstate"
