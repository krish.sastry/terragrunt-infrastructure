terragrunt = {
  terraform {
    source = "../../../modules//alb"
  }

  include = {
    path = "${find_in_parent_folders()}"
  }

  # Need the vpc module to be created
  dependencies {
    paths = ["../vpc"]
  }
}

# Module parameter
aws_region = "us-east-1"

alb_name = "example-alb"
