terragrunt = {
  remote_state {
    backend = "s3"
    config {
      bucket         = "terragrunt-lysimon"
      key            = "${path_relative_to_include()}/terraform.tfstate"
      region         = "us-east-1"
      encrypt        = true

      s3_bucket_tags {
        owner = "terragrunt"
        name  = "Terraform state storage"
      }

      # I disabled dynamodb for cost reason, feel free to enable it - probably better in production :)
      #dynamodb_table = "my-lock-table"
      #dynamodb_table_tags {
      #  owner = "terragrunt integration test"
      #  name  = "Terraform lock table"
      #}
    }
  }

  terraform {
    extra_arguments "bucket" {
    # Feel free to add as many layer as possible, such as environment.tfvar for instance
    commands = ["${get_terraform_commands_that_need_vars()}"]
    optional_var_files = [
        "${get_tfvars_dir()}/${find_in_parent_folders("account.tfvars", "ignore")}",
        "${get_tfvars_dir()}/${find_in_parent_folders("region.tfvars", "ignore")}",
        "${get_tfvars_dir()}/${find_in_parent_folders("global.tfvars", "ignore")}"
      ]
    }
  }
}

