# Configure aws provider here
provider "aws" {
  region = "${var.aws_region}"
}

# Create security group for the load balancer
resource "aws_security_group" "alb_sg" {
  name        = "${var.alb_name}"
  description = "Allow communication to this alb"
  vpc_id = "${data.terraform_remote_state.vpc.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    cidr_blocks = "${var.allowed_cidr}"
  }

  # Allow all egress rule
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "alb_sg_id" {
  value = "${aws_security_group.alb_sg.id}"
}

resource "aws_lb" "alb" {
  name               = "${var.alb_name}"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.alb_sg.id}"]
  subnets = ["${data.terraform_remote_state.vpc.public_subnets}"]

  enable_deletion_protection = false

  tags {
    Environment = "production"
  }
}

resource "aws_lb_listener" "alb" {
  load_balancer_arn = "${aws_lb.alb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"
    fixed_response {
      content_type = "text/plain"
      message_body = "Fixed response content"
      status_code = "200"
    }
  }
}

output "alb_arn" {
  value = "${aws_lb_listener.alb.arn}"
}

output "alb_dns_name" {
  value = "${aws_lb.alb.dns_name}"
}

resource "aws_route53_record" "www" {
  zone_id = "${var.route53_hosted_zone_id}"
  name    = "www"
  type    = "CNAME"
  ttl     = "300"
  records = ["${aws_lb.alb.dns_name}"]
}


resource "aws_lb_listener_rule" "static" {
  listener_arn = "${aws_lb_listener.alb.arn}"
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.test.arn}"
  }

  condition {
    field  = "path-pattern"
    values = ["/static/*"]
  }
}

resource "aws_lb_target_group" "test" {
  name     = "tf-example-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id = "${data.terraform_remote_state.vpc.vpc_id}"
}
